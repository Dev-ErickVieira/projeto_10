$(function(){
	
	$('nav a').hover(function(){
		$('nav li').removeClass('selected');
		$(this).parent().addClass('selected');
	});


	$('.menu-icon').click(function(){
		$('.nav-mobile').css('width','50%');
	});

	$('#closeBtn').click(function(){
		$('.nav-mobile').css('width','0');
	});

	$('header a').click(function(){
		var href = $(this).attr('href');

		var offSetTop = $(href).offset().top;

		$('html,body').animate({'scrollTop':offSetTop},1000);

		return false;
	});

	$('.nav-mobile > a ').click(function(){
		$('.nav-mobile').css('width','0');
	});


});